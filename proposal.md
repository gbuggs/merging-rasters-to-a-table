
An Arcpy Add-in to Merge Rasters to a Unified Table:
A variety of statistical tools require one-dimensional tabular data. I propose creating an ArcGIS Python tool to merge one or more Rasters to a table with XY coordinates, where cells from multiple Rasters are correctly aligned in the final table.


Background:
An earlier assignment required running regressions on spatial data using statistical tools that required tabular input. Although, the Rasters could be flattened to one dimension, analysis required multiple geolocated variables encoded across multiple Raster datasets.

This tool would take in multiple Rasters, then covert them to feature classes maintaining their data, and spatially merge all feature classes to form a unified, geo-located table that will be used for analysis.


Data and Parameters:
The required input would be a list of Rasters, as well as an optional feature class that controls extent, projection, and clipping. The tool will be able to take in any raster and will internally handle all the necessary transformations required to conduct merges.

Limitations:
ArcGIS does not have a problem to merge Rasters to Tables. The existing workflows of feature class conversion and spatial joins are too slow. So, no existing tools or readilt available workflows operate for large or complex datasets.

Pre-processing of Rasters pre-merge itself non-trivial and may require significant expermentation to optimize.


Solutions:
While feature class conversions and merges at large scales can be to slow, this tool would capitalize on the inherent grid structure of the raster to efficiently and accurately create a unified table by merging coordinates rather than polygons.

This tool automatically handles raster pre-processing for conversion and merging by accounting for alignment, resolution, and spatial reference.

This tool would allow ArcGIS tools to be easily used for statistical analysis and can pave the way for integration of ArcPy with other more general statistical Python packages.
