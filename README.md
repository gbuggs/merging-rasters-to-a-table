# Merging Rasters to a Table

Including an add-in to merge rasters to a unified table.

A variety of statistical tools require one-dimensional tabular data. I created an ArcGIS Python tool to merge one or more Raster datasets to a table with XY coordinates, where cells are from each Raster are aligned correctly in the final table. 

This tool takes multiple rasters and converts them to feature classes. Which maintains the integrity of the data and spatially merges all of the feature classes to create a geo-located table that is ready for analysis.

Data and Parameters: A list of Rasters is required, with an optional feature class for extent, projection, and clipping. This tool will be able to take in any raster and handle the transormations required to complete merges.

Limitations: This ArcGIS tool serves to speed up merging Rasters, a sometimes slow process with large Raster datasets. I found that single-band Rasters do not mend well with this tool, it may have been issues with the data itself but did not have time to download new Rasters and test.

Soultion: With the runtime for merging large rasters being quite slow. My tool will use the grid-structure of the Raster data to effectively and accurately develop a unified table by merging the XY coordinates instead of using polygons. This tool handles the pre-processing of the Raster data for converions and merging by accounting for the spatial reference, alignment, and resolution.  

Output: A geo-located unified table with merged Rasters in XY coordinates, created using a ArcGIS Python tool that does it much faster. 



 